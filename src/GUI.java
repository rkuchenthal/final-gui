import acm.program.*;
import acm.graphics.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.*;
import java.io.File;
import javax.sound.sampled.*;

public class GUI extends Program
{
    JTextField infixInput;
    JTextField postfixOutput;
    JTextField solutionOutput;
    
    public GUI()
    {
        start();
        setSize(800, 600);
    }
    
    public void init()
    {
        // Create a canvas and add it to the window
        GCanvas canvas = new GCanvas();
        add(canvas);
        canvas.setBackground(Color.GRAY);
        setTitle("CS 13 GUI Calculator");
        
        JLabel infixLabel = new JLabel("Infix:");
        JLabel postfixLabel = new JLabel("Postfix:");
        JLabel solutionLabel = new JLabel("Solution:");
        canvas.add(infixLabel, 20, 20);
        canvas.add(postfixLabel, 20, 60);
        canvas.add(solutionLabel, 20, 100);
          
        infixLabel.setForeground(Color.RED);
        postfixLabel.setForeground(Color.WHITE);
        solutionLabel.setForeground(Color.BLUE);
        
        infixInput = new JTextField();
        postfixOutput = new JTextField();
        solutionOutput = new JTextField();
        canvas.add(infixInput, 80, 20);
        canvas.add(postfixOutput, 80, 55);
        canvas.add(solutionOutput, 80, 95);
        infixInput.setSize(100, 20);
        postfixOutput.setSize(100, 20);
        solutionOutput.setSize(100, 20);
        infixInput.setBackground(Color.RED);
        postfixOutput.setBackground(Color.GRAY);
        solutionOutput.setBackground(Color.BLUE);
        ImageIcon mario = new ImageIcon("Images/mario block.jpg");
        JButton marioB = new JButton(mario);
        canvas.add(marioB, 50, 200);
       
         JButton convertButton = new JButton("Convert");
        JButton clearButton = new JButton("Clear");
        JButton ansButton = new JButton("ANS");
        canvas.add(convertButton, 20, 125);
        canvas.add(clearButton, 120, 125);
        canvas.add(ansButton, 220, 125);
        
        addActionListeners();
    }
    
    public void actionPerformed( ActionEvent e )
    {
        if (e.getActionCommand().equals("Convert"))
        {
            playCoin();
            // Get value from infixInput
            String infixStr = infixInput.getText();
            String infix = (infixStr);
            
            
            // Create infix to postfix converter
            SY n = new SY("" + infix);
            
            // Call convert
            String postfix = n.convert();
            
            // Put result into postfixOutput
            postfixOutput.setText("" + postfix);
            Postfix m = new Postfix(postfix);
            double ans = m.eval();
           
            
            //Eval postfix result and put into answer
            solutionOutput.setText("" + ans);
        }
        
        else if(e.getActionCommand().equals("Clear"))
        {
            playDie();
            infixInput.setText("");
            postfixOutput.setText("");
            solutionOutput.setText("");
        }
        else if(e.getActionCommand().equals("ANS"))
        {
            playJump();
            
            String infixStr = infixInput.getText();
            String infix = (infixStr);
            
            
            // Create infix to postfix converter
            SY n = new SY("" + infix);
            
            // Call convert
            String postfix = n.convert();
            
            // Put result into postfixOutput
            postfixOutput.setText("" + postfix);
            Postfix m = new Postfix(postfix);
            double ans = m.eval();
           
            
            //Eval postfix result and put into answer
            solutionOutput.setText("" + ans);
            
            infixInput.setText("" + ans);
            postfixOutput.setText("");
            solutionOutput.setText("");
        }
        }
        public void playCoin()
        {
            try
          {
             File file = new File("Sound/smb_coin.wav");
             Clip clip = AudioSystem.getClip();
             clip.open(AudioSystem.getAudioInputStream(file));
             clip.start();
          }
          catch(Exception e)
          {
          }
        }
         public void playJump()
        {
            try
          {
             File file = new File("Sound/smb_jump.wav");
             Clip clip = AudioSystem.getClip();
             clip.open(AudioSystem.getAudioInputStream(file));
             clip.start();
          }
          catch(Exception e)
          {
          }
        }
         public void playDie()
        {
            try
          {
             File file = new File("Sound/smb_die.wav");
             Clip clip = AudioSystem.getClip();
             clip.open(AudioSystem.getAudioInputStream(file));
             clip.start();
          }
          catch(Exception e)
          {
          }
        }

            public static void main(String[] args)
            {
                GUI g = new GUI();
            }
    }















