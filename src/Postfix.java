import java.util.Stack;

public class Postfix
{
    String expr;
    Stack<Double> stack;
    
    public Postfix(String e)
    {
        expr = e;
        stack = new Stack<Double>();
    }
    
    public double eval()
    {
        String[] tokens = expr.split(" ");
        
        for (int i = 0; i < tokens.length; i++)
        {
            if (tokens[i].equals("+"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double sum = a + b;
                stack.push(sum);
            }
            else if (tokens[i].equals("*"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double sum = a * b;
                stack.push(sum);
            }
            else if (tokens[i].equals("-"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double difference = b - a;
                stack.push(difference);
            }
            else if(tokens[i].equals("/"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double quotient = b / a;
                stack.push(quotient);
            }
            else if(tokens[i].equals("^"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double power = Math.pow( b, a );
                stack.push(power);
            }
  
            else if(tokens[i].equals("sqrt"))
            {
                double a = stack.pop();
                // double b = stack.pop();
                double root = Math.sqrt(a);
                stack.push(root);
            }
            
            else if(tokens[i].equals("sin"))
            {
                double a = stack.pop();
                double sin1 = Math.sin(a);
                stack.push(sin1);
            }
            else if(tokens[i].equals("cos"))
            {
                double a = stack.pop();
                double cos1 = Math.cos(a);
                stack.push(cos1);
            }
            else if (tokens[i].equals("tan"))
            {
                double a = stack.pop();
                double tan1 = Math.tan(a);
                stack.push(tan1);
            }
            else if(tokens[i].equals("ln"))
            {
                double a = stack.pop();
                double natLog = Math.log(a);
                stack.push(natLog);   
            }
            else if(tokens[i].equals("log"))
            {
                double a = stack.pop();
                double log10 = Math.log10(a);
                stack.push(log10);   
            }
            else
            {
                stack.push(Double.parseDouble(tokens[i]));
            }
        }
        
        return stack.pop();
    }
}